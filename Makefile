include .env

# Pass parameters to docker-compose
CURRENT_USER := $(shell id -un)
CURRENT_USERID := $(shell id -u)
export CURRENT_USER
export CURRENT_USERID

help:
	@echo ""
	@echo "Config File: .env"
	@echo ""
	@echo "Usage:"
	@echo "make init       - Configure Services, run this only first time"
	@echo "make start      - Start Services"
	@echo "make stop       - Stop Services"
	@echo "make up         - Create and Start Services"
	@echo "make down       - Stop and Delete Services"
	@echo "make restart    - Restart Server"
	@echo "make sh         - Interactive mode on php image"
	@echo "make build      - Build Container image"


# For Development
init:
	@# Change nginx.conf file
	@sed -i "s/server_name .*/server_name ${HOST};/" docker/nginx/default.conf

	@if [ -d ${DB_PATH} ]; then \
		read -p "Remove DB folder? [y/N] " ans && ans=$${ans:-N}; \
		if [ $${ans} = y ] || [ $${ans} = Y ]; then \
		  	printf $(_SUCCESS) "Remove" ; \
			sudo rm -rf ${DB_PATH} ; \
		else \
			printf $(_SUCCESS) "NO" ; \
		fi \
	fi



	@#----- CODE TO DELETE AFTER FIRST INSTALL
	@if [ -d ${APP_PATH} ]; then \
		read -p "Remove ${APP_PATH} folder? [y/N] " ans && ans=$${ans:-N}; \
		if [ $${ans} = y ] || [ $${ans} = Y ]; then \
		  	printf $(_SUCCESS) "Remove" ; \
			sudo rm -rf ${APP_PATH} ; \
		else \
		  printf $(_SUCCESS) "NO" ; \
		fi \
	fi

	@mkdir -p ${APP_PATH}
	@# Change owner and group. Group 82 is www-data on docker
	@sudo chown -R ${USER}:82 ${APP_PATH}

	@# Start Container
	@docker compose -f docker-compose.yml up -d

	@echo ""
	@if [ -d ${APP_PATH}/wp-admin ]; then \
		read -p "Install Wordpress? [y/N] " INST && INST=$${INST:-N} ; \
	else \
		INST=Y ; \
	fi ; \
	if [ $${INST} = y ] || [ $${INST} = Y ]; then \
		printf $(_SUCCESS) "YES" "Installing Wordpress..." ; \
		wget https://wordpress.org/latest.zip ; \
        unzip latest.zip -d data/ ; \
        rm latest.zip ; \
        mv ${APP_PATH}/wp-config-sample.php ${APP_PATH}/wp-config.php ; \
        sed -i 's/database_name_here/'"${DB_DATABASE}"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/username_here/'"${DB_USERNAME}"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/password_here/'"${DB_PASSWORD}"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/localhost/${DB_HOST}/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'AUTH_KEY',".*"'put your unique phrase here'"'/'"'AUTH_KEY', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'SECURE_AUTH_KEY',".*"'put your unique phrase here'"'/'"'SECURE_AUTH_KEY', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'LOGGED_IN_KEY',".*"'put your unique phrase here'"'/'"'LOGGED_IN_KEY', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'NONCE_KEY',".*"'put your unique phrase here'"'/'"'NONCE_KEY', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'AUTH_SALT',".*"'put your unique phrase here'"'/'"'AUTH_SALT', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'SECURE_AUTH_SALT',".*"'put your unique phrase here'"'/'"'SECURE_AUTH_SALT', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'LOGGED_IN_SALT',".*"'put your unique phrase here'"'/'"'LOGGED_IN_SALT', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
        sed -i 's/'"'NONCE_SALT',".*"'put your unique phrase here'"'/'"'NONCE_SALT', '""$$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)""'"'/g' ${APP_PATH}/wp-config.php ; \
		sudo chown -R ${USER}:82 ${APP_PATH} ; \
	else \
		printf $(_SUCCESS) "NO" "Finish." ; \
	fi
	@#----- FIN CODE



	@# Add host to /etc/hosts file
	@if [ -n "$$(grep -P ${HOST} /etc/hosts)" ]; then \
		echo "Site ${HOST} already exists in hosts" ; \
	else \
	    echo ; \
		echo "Adding site ${HOST} to your hosts" ; \
		sudo echo "127.0.0.1 ${HOST}" >> /etc/hosts ; \
    fi

	@echo ""
	@printf $(_SUCCESS) "Web ready:" "http://${HOST}:${HOST_PORT}"
	@echo ""

# For Development
start:
	@docker compose -f docker-compose.yml start
	@printf $(_SUCCESS) "Web ready:" "http://${HOST}:${HOST_PORT}"

stop:
	@docker compose -f docker-compose.yml stop

sh:
	@docker exec -it ${APP_SLUG} sh

up:
	@docker compose -f docker-compose.yml up -d
	@printf $(_SUCCESS) "Web ready:" "http://${HOST}:${HOST_PORT}"

down:
	@docker compose -f docker-compose.yml down

restart:
	@docker compose -f docker-compose.yml down
	@docker compose -f docker-compose.yml up -d

rebuild:
	docker compose -f docker-compose.yml stop
	docker compose -f docker-compose.yml pull
	docker compose -f docker-compose.yml rm --force app
	docker compose -f docker-compose.yml build --no-cache --pull
	docker compose -f docker-compose.yml up -d --force-recreate

# For production
build:
	sudo chown -R ${USER} ${DB_PATH}
	docker compose -f docker-compose-prod.yml -p ${APP_SLUG} build
	#docker compose -f docker-compose.yml up -d
	#docker compose -f docker-compose.yml down


_WARN := "\033[33m[%s]\033[0m %s\n"  # Yellow text for "printf"
_SUCCESS := "\033[32m[%s]\033[0m %s\n" # Green text for "printf"
_ERROR := "\033[31m[%s]\033[0m %s\n" # Red text for "printf"