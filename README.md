# Wordpress boilerplate
## Steps for create Laravel Project
* `.env` - Edit configuration file
* `make init` - Setup, first time only

## Work steps
* `make start` - Start services
* `make stop` - Stop services
## Build project
* `make build` - Build for Production
